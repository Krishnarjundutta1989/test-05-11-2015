//
//  ViewController.m
//  2015
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
    NSString *formatedaddress;
    CLLocationCoordinate2D position;
    NSString *City;
    NSString *state;
    NSString *country;
    NSString *Name;
    NSString *State;
    GMSMarker *marker;
    NSString *latitude;
    NSString *longitude;
    NSString *address;
    NSString *massage;
    CLLocation *currentLocation;
    NSMutableArray *arrayForDetails;
    NSMutableArray *destarrayForDetails;

}
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;
@property (strong, nonatomic) IBOutlet UINavigationItem *navBar;

@end

@implementation ViewController
@synthesize googleMap;

- (void)viewDidLoad {
    [super viewDidLoad];

    arrayForDetails = [NSMutableArray new];
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager startUpdatingLocation];
    [manager requestWhenInUseAuthorization];
    googleMap.delegate = self;
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Unable To Get Your Current Location Please Tap For Get Details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    currentLocation = newLocation;
//
//    
//    if (currentLocation != nil) {
//        
//        position = currentLocation.coordinate;
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
//                                                                longitude:currentLocation.coordinate.longitude
//                                                                     zoom:10.0];
//        [googleMap animateToCameraPosition:camera];
//        marker = [GMSMarker markerWithPosition:position];
//        marker.title = [NSString stringWithFormat:@"%@",formatedaddress];
//        
//        marker.map = googleMap;
//        [self getaddress];
//        
//        
//    }
    //[manager stopUpdatingLocation];
//}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    position = coordinate;
    
    // GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
    //longitude:coordinate.longitude
    //  zoom:2.0];
    //  [_googleMap animateToCameraPosition:camera];
    marker = [GMSMarker markerWithPosition:position];
    [self getaddress];
    
    
    
    
}

-(void) getaddress  {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [arrayForDetails removeAllObjects];

    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:position.latitude
                                                        longitude:position.longitude];



    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                           
                           if (addressDictionary[@"Country"]!= NULL){
                               country = addressDictionary[@"Country"];
                               NSLog(@"%@",country);
                               
                               if ([country isEqualToString:@"India"]) {
                                   massage = @"You Are In India";
                                   [arrayForDetails addObject:[NSString stringWithFormat:@"                 %@",massage]];

                                   //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",massage]];
                               }
                               else {
                                   massage = @"You Are OutSide India";
                                   //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",massage]];
                                   [arrayForDetails addObject:[NSString stringWithFormat:@"                 %@",massage]];


                               }
                               
                               //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",country]];
                               [arrayForDetails addObject:[NSString stringWithFormat:@"Country :     %@",country]];


                           }
                           else{
                               country = @"NA";
                               NSLog(@"%@",country);
                               //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",country]];
                               [arrayForDetails addObject:[NSString stringWithFormat:@"Country :     %@",country]];

                               
                               
                           }
                          if (addressDictionary[@"Name"]!= NULL) {
                               Name = addressDictionary[@"Name"];
                               NSLog(@"%@",Name);
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",Name]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"Name :        %@",Name]];


                           }
                          else{
                              Name = @"NA";
                              NSLog(@"%@",Name);
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",Name]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"Name :        %@",Name]];

                              

                          }
                          if (addressDictionary[@"State"]!= NULL) {
                              State = addressDictionary[@"State"];
                              NSLog(@"%@",State);
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",State]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"State :         %@",State]];


                          }
                          else{
                              State = @"NA";
                              NSLog(@"%@",State);
                             // [arrayForDetails addObject:[NSString stringWithFormat:@"%@",State]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"State :         %@",State]];

                              
                          }
                          if (addressDictionary[@"City"]!= NULL) {
                              City = addressDictionary[@"City"];
                              NSLog(@"%@",City);
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",City]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"City :           %@",City]];

                              marker = [GMSMarker markerWithPosition:position];
                              marker.title = [NSString stringWithFormat:@"%@",City];
                              
                              marker.map = googleMap;

                          }
                          else{
                              City = @"NA";
                              NSLog(@"%@",City);
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",City]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"City :           %@",City]];

                              marker = [GMSMarker markerWithPosition:position];
                              marker.title = [NSString stringWithFormat:@"%@",City];
                              
                              marker.map = googleMap;

                              
                          }

                          if (addressDictionary[@"FormattedAddressLines"] != NULL){
                              NSArray *FormattedAddressLines = addressDictionary[@"FormattedAddressLines"];
                            
                                 formatedaddress = [FormattedAddressLines objectAtIndex:0];
                              //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",formatedaddress]];
                              [arrayForDetails addObject:[NSString stringWithFormat:@"Adress :       %@",formatedaddress]];


                              NSLog(@"%@",arrayForDetails);
                          }

                       }
                       
                       
                   }];
    latitude = [NSString stringWithFormat:@"%f",position.latitude];
    //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",latitude]];
    [arrayForDetails addObject:[NSString stringWithFormat:@"Latitude  :    %@",latitude]];

    longitude = [NSString stringWithFormat:@"%f",position.longitude];
    //[arrayForDetails addObject:[NSString stringWithFormat:@"%@",longitude]];
    [arrayForDetails addObject:[NSString stringWithFormat:@"Longitude :   %@",longitude]];


    
}

- (void) didPan:(UIPanGestureRecognizer*) gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        googleMap.settings.scrollGestures = true;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (gestureRecognizer.numberOfTouches > 1)
    {
        googleMap.settings.scrollGestures = false;
    }
    else
    {
        googleMap.settings.scrollGestures = true;
    }
    return true;
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"GoToDestination"]) {
        if (arrayForDetails.count == 0) {
            [self errormassage];
            
        }
        else{
            GetDetailsViewController *VC = segue.destinationViewController;
            VC.destarrayForDetails = arrayForDetails;
            [googleMap clear];
            

    }
        }
   
}
-(void)errormassage {
    UIAlertView *aleart = [[UIAlertView alloc]initWithTitle:@"" message:@"Please Select Location First" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [aleart show];
}

    - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
