//
//  GetDetailsViewController.m
//  2015
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "GetDetailsViewController.h"

@interface GetDetailsViewController (){
    NSMutableArray *arrayForDisplay;
    NSString *destmassage;
    NSString *destName;
    NSString *destCity;
    NSString *destLat;
    NSString *destlon;
    NSString *destState;
    NSString *destaddress;
    NSString *destCountry;
    NSMutableArray *arrayForDetailsTable;
}
@property (strong, nonatomic) IBOutlet UITableView *tblForDestDetails;

@end

@implementation GetDetailsViewController
@synthesize destarrayForDetails;
@synthesize tblForDestDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
//    arrayForDetailsTable = [NSMutableArray new];
//   destmassage = [destarrayForDetails objectAtIndex:2];
//   destName = [destarrayForDetails objectAtIndex:4];
//   destCountry = [destarrayForDetails objectAtIndex:3];
//   destCity = [destarrayForDetails objectAtIndex:6];
//   destLat = [destarrayForDetails objectAtIndex:0];
//   destlon = [destarrayForDetails objectAtIndex:1];
//   destState = [destarrayForDetails objectAtIndex:5];
//   destaddress = [destarrayForDetails objectAtIndex:7];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"                 %@",destmassage]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"Name :        %@",destName]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"State :         %@",destState]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"City :           %@",destCity]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"Country :     %@",destCountry]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"Latitude  :    %@",destLat]];
//    [arrayForDetailsTable addObject:[NSString stringWithFormat:@"Longitude :   %@",destlon]];
//   [arrayForDetailsTable addObject:[NSString stringWithFormat:@"Adress :       %@",destaddress]];
    NSLog(@"%@",arrayForDetailsTable);



    [tblForDestDetails reloadData];

    
    
    
    // Do any additional setup after loading the view.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  destarrayForDetails.count;
    
    
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" ];
    cell.textLabel.text = destarrayForDetails [indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row %2) {
        [cell setBackgroundColor:[UIColor grayColor]] ;
    }
    else{
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
